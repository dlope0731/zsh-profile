# zsh Profile

My zsh profile plus script to setup a fresh linux machine with my terminal settings and packages needed for my fresh machine.

To install:

```bash
sh -c "$(curl -fsSL https://gitlab.com/dlope0731/zsh-profile/-/raw/master/setup.sh)"
```
