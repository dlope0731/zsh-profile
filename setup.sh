#!/bin/bash

# Update System First

sudo apt update; sudo apt upgrade

# Install Rust (Including rust-analysis)

curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --profile complete

#Install vscode

sudo apt install code

#Install font for terminal theme
git clone https://github.com/abertsch/Menlo-for-Powerline.git
pushd Menlo-for-Powerline
mv *.tff /usr/share/fonts
popd
#Delete repo as we don't need it anymore
rm -rf Menlo-for-Powerline 

#Install zsh

sudo apt install zsh

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

#Download my zsh config

wget -O ~/.zshrc https://gitlab.com/dlope0731/zsh-profile/-/raw/master/.zshrc

#Print completion message
echo "Finished setting up your machine...enjoy!"


